import base64
import string
import json
import requests
import datetime
from lxml import etree
from odoo import fields, models, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from odoo.tools import float_is_zero, float_compare
from reportlab.graphics.barcode import createBarcodeDrawing, getCodes
from reportlab.lib.units import mm
from . import amount_to_text_es_MX
from suds.client import Client
import zeep
from odoo.tools.safe_eval import safe_eval

# mapping invoice type to refund type
TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Vendor Bill
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Vendor Refund
}


class account_invoice_cfdi(models.TransientModel):

    _name='account.invoice.cfdi'

    cfdi = fields.Many2one('account.invoice', string="CFDI")

    @api.one
    def timbrar_cfdi(self):
        res={}
        for record in self:
            if record.cfdi.estado_factura == 'factura_correcta':
                raise UserError(_('La factura ya tiene un CFDI generado'))
            if record.cfdi.estado_factura == 'factura_cancelada':
                raise UserError(_('No puede generar un CFDI de una factura con CFDI cancelado.'))
            url = record.cfdi.company_id.http_factura or False
            if not url:
                raise UserError(_('La compania no tiene configurado la URL del Web Service.'))
            #cliente_cfdi = Client(url, timeout=600)
            cliente_cfdi = zeep.Client(wsdl=url)
            response = cliente_cfdi.service.procesaDocto(record.cfdi.id, self.env.cr.dbname, 'ODOO')
            record.cfdi.write({'estado_factura':response['codigo']})

class account_invoice_line_invoice(models.Model):

    _name='account.invoice.line.invoice'

    nc_id = fields.Many2one('account.invoice', string="Nota Credito", ondelete='cascade', index=True, store=True)
    invoice_id = fields.Many2one('account.invoice', string="Factura", ondelete='restrict', index=True, store=True)
    name = fields.Char(string="UUID", related='invoice_id.folio_fiscal', store=True)
    by_payment = fields.Boolean(string="Pago de factura?", default=False, store=True)

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None, description=None, journal_id=None):
        """ Prepare the dict of values to create the new refund from the invoice.
            This method may be overridden to implement custom
            refund generation (making sure to call super() to establish
            a clean extension chain).

            :param record invoice: invoice to refund
            :param string date_invoice: refund creation date from the wizard
            :param integer date: force date from the wizard
            :param string description: description of the refund from the wizard
            :param integer journal_id: account.journal from the wizard
            :return: dict of value to create() the refund
        """
        values = {}
        for field in self._get_refund_copy_fields():
            if invoice._fields[field].type == 'many2one':
                values[field] = invoice[field].id
            else:
                values[field] = invoice[field] or False

        values['invoice_line_ids'] = self._refund_cleanup_lines(invoice.invoice_line_ids)

        tax_lines = invoice.tax_line_ids
        values['tax_line_ids'] = self._refund_cleanup_lines(tax_lines)

        if journal_id:
            journal = self.env['account.journal'].browse(journal_id)
        elif invoice['type'] == 'in_invoice':
            journal = self.env['account.journal'].search([('type', '=', 'purchase')], limit=1)
        else:
            journal = self.env['account.journal'].search([('type', '=', 'sale')], limit=1)
        values['journal_id'] = journal.id

        values['type'] = TYPE2REFUND[invoice['type']]
        values['date_invoice'] = date_invoice or fields.Date.context_today(invoice)
        values['state'] = 'draft'
        values['number'] = False
        values['origin'] = invoice.number
        values['refund_invoice_id'] = invoice.id
        values['forma_pago'] = invoice.forma_pago.id
        values['methodo_pago'] = invoice.methodo_pago.id
        values['uso_cfdi'] = invoice.uso_cfdi.id
        values['vde_invoice_line'] = [(0,0,{'invoice_id':invoice.id})]
        values['tipo_comprobante'] = 'E'

        if date:
            values['date'] = date
        if description:
            values['name'] = description
        return values



    @api.multi
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None):
        new_invoices = self.browse()
        for invoice in self:
            # create the new invoice
            values = self._prepare_refund(invoice, date_invoice=date_invoice, date=date,
                                    description=description, journal_id=journal_id)
            refund_invoice = self.create(values)
            invoice_type = {'out_invoice': ('customer invoices refund'),
                'in_invoice': ('vendor bill refund')}
            message = _("This %s has been created from: <a href=# data-oe-model=account.invoice data-oe-id=%d>%s</a>") % (invoice_type[invoice.type], invoice.id, invoice.number)
            refund_invoice.message_post(body=message)
            new_invoices += refund_invoice
        return new_invoices

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        account_id = False
        payment_term_id = False
        fiscal_position = False
        bank_id = False
        warning = {}
        domain = {}
        company_id = self.company_id.id
        p = self.partner_id if not company_id else self.partner_id.with_context(force_company=company_id)
        type = self.type
        if p:
            rec_account = p.property_account_receivable_id
            pay_account = p.property_account_payable_id
            if not rec_account and not pay_account:
                action = self.env.ref('account.action_account_config')
                msg = _('Cannot find a chart of accounts for this company, You should configure it. \nPlease go to Account Configuration.')
                raise RedirectWarning(msg, action.id, _('Go to the configuration panel'))

            if type in ('out_invoice', 'out_refund'):
                account_id = rec_account.id
                payment_term_id = p.property_payment_term_id.id
            else:
                account_id = pay_account.id
                payment_term_id = p.property_supplier_payment_term_id.id

            delivery_partner_id = self.get_delivery_partner_id()
            fiscal_position = self.env['account.fiscal.position'].get_fiscal_position(self.partner_id.id, delivery_id=delivery_partner_id)

            # If partner has no warning, check its company
            if p.invoice_warn == 'no-message' and p.parent_id:
                p = p.parent_id
            if p.invoice_warn != 'no-message':
                # Block if partner only has warning but parent company is blocked
                if p.invoice_warn != 'block' and p.parent_id and p.parent_id.invoice_warn == 'block':
                    p = p.parent_id
                warning = {
                    'title': _("Warning for %s") % p.name,
                    'message': p.invoice_warn_msg
                    }
                if p.invoice_warn == 'block':
                    self.partner_id = False
#############Code VDE
            if p.forma_pago:
                self.forma_pago = p.forma_pago
            else:
                self.forma_pago = False
            if p.methodo_pago:
                self.methodo_pago = p.methodo_pago
            else:
                self.methodo_pago = False
            if p.uso_cfdi:
                self.uso_cfdi = p.uso_cfdi
            else:
                self.uso_cfdi = False
############Code VDE
        self.account_id = account_id
        self.payment_term_id = payment_term_id
        self.date_due = False
        self.fiscal_position_id = fiscal_position

        if type in ('in_invoice', 'out_refund'):
            bank_ids = p.commercial_partner_id.bank_ids
            bank_id = bank_ids[0].id if bank_ids else False
            self.partner_bank_id = bank_id
            domain = {'partner_bank_id': [('id', 'in', bank_ids.ids)]}

        res = {}
        if warning:
            res['warning'] = warning
        if domain:
            res['domain'] = domain
        return res




    @api.one
    @api.depends('type')
    def _get_tipo_factura(self):
        print ("entra _get_tipo_factura ",self.type)
        if self.type=='out_invoice':
            self.tipo_comprobante = 'I'
        elif self.type=='out_refund':
            self.tipo_comprobante = 'E'
        elif self.type=='in_invoice':
            self.tipo_comprobante = 'I'
        elif self.type=='in_refund':
            self.tipo_comprobante = 'E'
        else:
            self.tipo_comprobante = 'T'

    nc_id = fields.Many2one('account.invoice', 'NC Relacionada', store=True)
    vde_invoice_line = fields.One2many('account.invoice.line.invoice', 'nc_id', string="Facturas relacionadas", store=True)


    factura_cfdi = fields.Boolean('Factura CFDI', copy=False, store=True)
    tipo_comprobante = fields.Selection(selection=[('I', 'Ingreso'), ('E', 'Egreso'), ('T', 'Traslado')], string=_('Tipo de comprobante'), default=lambda self: self.env.context.get('tipo', False), store=True)
    forma_pago = fields.Many2one('c.formapago', string="Forma de pago", store=True)
    #forma_pago = fields.Selection(selection=[('01', '01 - Efectivo'),
     #('02', '02 - Cheque nominativo'),
     #('03', '03 - Transferencia electr\xc3\xb3nica de fondos'),
     #('04', '04 - Tarjeta de Cr\xc3\xa9dito'),
     #('05', '05 - Monedero electr\xc3\xb3nico'),
     #('06', '06 - Dinero electr\xc3\xb3nico'),
     #('08', '08 - Vales de despensa'),
     #('12', '12 - Daci\xc3\xb3n en pago'),
     #('13', '13 - Pago por subrogaci\xc3\xb3n'),
     #('14', '14 - Pago por consignaci\xc3\xb3n'),
     #('15', '15 - Condonaci\xc3\xb3n'),
     #('17', '17 - Compensaci\xc3\xb3n'),
     #('23', '23 - Novaci\xc3\xb3n'),
     #('24', '24 - Confusi\xc3\xb3n'),
     #('25', '25 - Remisi\xc3\xb3n de deuda'),
     #('26', '26 - Prescripci\xc3\xb3n o caducidad'),
     #('27', '27 - A satisfacci\xc3\xb3n del acreedor'),
     #('28', '28 - Tarjeta de d\xc3\xa9bito'),
     #('29', '29 - Tarjeta de servicios'),
     #('30', '30 - Aplicaci\xc3\xb3n de anticipos'),
     #('99', '99 - Por definir')], string=_('Forma de pago'))
    #methodo_pago = fields.Selection(selection=[('PUE', _('Pago en una sola exhibici\xc3\xb3n')), ('PPD', _('Pago en parcialidades o diferido'))], string=_('M\xc3\xa9todo de pago'))
    methodo_pago = fields.Many2one('c.metodopago', string="Método de pago", store=True)
    uso_cfdi = fields.Many2one('c.usocfdi', string="Uso de CFDI", store=True)
    #uso_cfdi = fields.Selection(selection=[('G01', _('Adquisici\xc3\xb3n de mercanc\xc3\xadas')),
     #('G02', _('Devoluciones, descuentos o bonificaciones')),
     #('G03', _('Gastos en general')),
     #('I01', _('Construcciones')),
     #('I02', _('Mobiliario y equipo de oficina por inversiones')),
     #('I03', _('Equipo de transporte')),
     #('I04', _('Equipo de c\xc3\xb3mputo y accesorios')),
     #('I05', _('Dados, troqueles, moldes, matrices y herramental')),
     #('I08', _('Otra maquinaria y equipo')),
     #('D01', _('Honorarios m\xc3\xa9dicos, dentales y gastos hospitalarios')),
     #('D02', _('Gastos m\xc3\xa9dicos por incapacidad o discapacidad')),
     #('D03', _('Gastos funerales')),
     #('D04', _('Donativos')),
     #('D07', _('Primas por seguros de gastos m\xc3\xa9dicos')),
     #('D08', _('Gastos de transportaci\xc3\xb3n escolar obligatoria')),
     #('D10', _('Pagos por servicios educativos (colegiaturas)')),
     #('P01', _('Por definir'))], string=_('Uso CFDI (cliente)'))
    xml_invoice_link = fields.Char(string=_('XML Invoice Link'), store=True)
    estado_factura = fields.Selection(selection=[('factura_no_generada', 'CFDI No generado',),
     ('factura_correcta', 'CFDI generado correctamente'),
     ('problemas_factura', 'Problemas en el CFDI'),
     ('factura_cancelada', 'CFDI cancelado')], string=_('Estado de factura'), default='factura_no_generada', readonly=True, copy=False)
    pdf_cdfi_invoice = fields.Binary('CDFI Invoice', store=True)
    qrcode_image = fields.Binary('QRCode', store=True)
    regimen_fiscal = fields.Selection(selection=[('601', _('General de Ley Personas Morales')),
     ('603', _('Personas Morales con Fines no Lucrativos')),
     ('605', _('Sueldos y Salarios e Ingresos Asimilados a Salarios')),
     ('606', _('Arrendamiento')),
     ('608', _('Dem\xc3\xa1s ingresos')),
     ('609', _('Consolidaci\xc3\xb3n')),
     ('610', _('Residentes en el Extranjero sin Establecimiento Permanente en M\xc3\xa9xico')),
     ('611', _('Ingresos por Dividendos (socios y accionistas)')),
     ('612', _('Personas F\xc3\xadsicas con Actividades Empresariales y Profesionales')),
     ('614', _('Ingresos por intereses')),
     ('616', _('Sin obligaciones fiscales')),
     ('620', _('Sociedades Cooperativas de Producci\xc3\xb3n que optan por diferir sus ingresos')),
     ('621', _('Incorporaci\xc3\xb3n Fiscal')),
     ('622', _('Actividades Agr\xc3\xadcolas, Ganaderas, Silv\xc3\xadcolas y Pesqueras')),
     ('623', _('Opcional para Grupos de Sociedades')),
     ('624', _('Coordinados')),
     ('628', _('Hidrocarburos')),
     ('607', _('R\xc3\xa9gimen de Enajenaci\xc3\xb3n o Adquisici\xc3\xb3n de Bienes')),
     ('629', _('De los Reg\xc3\xadmenes Fiscales Preferentes y de las Empresas Multinacionales')),
     ('630', _('Enajenaci\xc3\xb3n de acciones en bolsa de valores')),
     ('615', _('R\xc3\xa9gimen de los ingresos por obtenci\xc3\xb3n de premios'))], string=_('R\xc3\xa9gimen Fiscal'))
    numero_cetificado = fields.Char(string=_('Numero de cetificado'))
    cetificaso_sat = fields.Char(string=_('Cetificao SAT'), copy=False)
    folio_fiscal = fields.Char(string=_('Folio Fiscal'), readonly=True, copy=False)
    fecha_certificacion = fields.Char(string=_('Fecha y Hora Certificación'))
    cadena_origenal = fields.Char(string=_('Cadena Origenal del Complemento digital de SAT'), copy=False)
    selo_digital_cdfi = fields.Char(string=_('Selo Digital del CDFI'), copy=False)
    selo_sat = fields.Char(string=_('Selo del SAT'), copy=False)
    moneda = fields.Char(string=_('Moneda'), store=True)
    #tipocambio = fields.Char(string=_('TipoCambio'))
    tipocambio = fields.Float(string=_('TipoCambio'), compute='_get_tc',store=True)
    folio = fields.Char(string=_('Folio'))
    version = fields.Char(string=_('Version'))
    #number_folio = fields.Char(string=_('Folio'), compute='_get_number_folio')
    amount_to_text = fields.Char('Amount to Text', compute='_get_amount_to_text', size=256, help='Amount of the invoice in letter', store=True)
    qr_value = fields.Char(string=_('QR Code Value'), copy=False)
    invoice_datetime = fields.Char(string=_('11/12/17 12:34:12'))
    rfc_emisor = fields.Char(string=_('RFC'))
    name_emisor = fields.Char(string=_('Name'))
    serie_emisor = fields.Char(string=_('A'))
    tipo_relacion = fields.Many2one('c.tiporelacion', string="Tipo Relacion")
    #tipo_relacion = fields.Selection(selection=[('01', 'Nota de cr\xc3\xa9dito de los documentos relacionados'),
     #('02', 'Nota de d\xc3\xa9bito de los documentos relacionados'),
     #('03', 'Devoluci\xc3\xb3n de mercanc\xc3\xada sobre facturas o traslados previos'),
     #('04', 'Sustituci\xc3\xb3n de los CFDI previos'),
     #('05', 'Traslados de mercanc\xc3\xadas facturados previamente'),
     #('06', 'Factura generada por los traslados previos'),
     #('07', 'CFDI por aplicaci\xc3\xb3n de anticipo')], string=_('Tipo relaci\xc3\xb3n'))
    invoice_related = fields.Many2one('account.invoice', store=True )
    uuid_relacionado = fields.Char(string=_('CFDI Relacionado'), copy=False)
    confirmacion = fields.Char(string=_('Confirmación'))
    mensaje_pac = fields.Text(string="Mensaje de Pac")
    fecha_emision = fields.Char(string=_('Fecha y Hora Emisión'))
    pac_certificacion = fields.Char(string=_('RFC PAC'))
    descuento_total = fields.Float(string="Descuento", compute='_compute_amount',store=True)
    amount_tax = fields.Float(compute='_compute_amount',store=True)
    amount_total = fields.Float(compute='_compute_amount',store=True)
    amount_untaxed = fields.Float(compute='_compute_amount',store=True)
    amount_tax1 = fields.Float(string="impuestos1", compute='_compute_amount',store=True)
    amount_tax2 = fields.Float(string="impuestos2", compute='_compute_amount',store=True)

    total_antes_descuento = fields.Float(string="Importe sin descuento", compute='_compute_amount', store=True)
    update_id = fields.One2many('account.invoice.update', 'invoice_id', string="Intentos", store=True)
    #mostrador = fields.Boolean(string="Venta a mostrador?", default=False)
    sustitucion = fields.Boolean(string="Es una sustitucion?", default=False)


    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        amount_untaxed = sum(line.price_subtotal_sn for line in self.invoice_line_ids)
        self.amount_untaxed = amount_untaxed
        #self.total_antes_descuento = sum((line.price_subtotal_sn) for line in self.invoice_line_ids)
        #self.descuento_total = sum(round(((line.price_unit*line.quantity) * (line.discount/100)),2) for line in self.invoice_line_ids)
        total_antes_descuento = sum((line.price_subtotal_sn) for line in self.invoice_line_ids)
        self.total_antes_descuento = total_antes_descuento
        self.descuento_total = sum(line.discount_line for line in self.invoice_line_ids)
        #self.descuento_total = total_antes_descuento - amount_untaxed
        #self.amount_untaxed = total_antes_descuento - sum(line.discount_line for line in self.invoice_line_ids)

        self.amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids)
        #self.amount_tax = 0

        #self.amount_tax = sum(line.line_tax1 for line in self.invoice_line_ids)
        self.amount_tax1 = sum(line.line_tax1 for line in self.invoice_line_ids)
        self.amount_tax2 = sum(line.line_tax2 for line in self.invoice_line_ids)

        self.amount_total = abs(self.amount_untaxed + self.amount_tax - self.descuento_total)
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign
        self.amount_untaxed = amount_untaxed - self.descuento_total

    @api.multi
    def invoice_validate(self):
        if self.type=='out_invoice':
            return self.write({'state': 'open','tipo_comprobante':'I'})
        elif self.type=='in_refund':
            return self.write({'state': 'open'})
        else:
            return self.write({'state': 'open'})


    @api.multi
    def invoice_get_serie_folio(self):
        print ("entrandooo_val")
        for invoice in self:
            if invoice.type=='out_invoice' or invoice.type=='out_refund':
                invoice.write({'serie_emisor':str.split(invoice.number,'-')[0], 'folio':str.split(invoice.number,'-')[1]})

    @api.multi
    def action_invoice_open(self):
        print ("entrandooo")
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        print ("esto es to_open_invoices ", to_open_invoices)
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        to_open_invoices.invoice_get_serie_folio()
        return to_open_invoices.invoice_validate()

    @api.depends('date_invoice', 'currency_id')
    @api.one
    def _get_tc(self):
        #self.amount_to_text = amount_to_text_es_MX.get_amount_to_text(self, self.amount_total, 'es_cheque', self.currency_id.name)
        if self.date_invoice:
            invoice_date_datetime = self.date_invoice
        else:
            today = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            invoice_date_datetime = datetime.datetime.strptime(today, '%Y-%m-%d %H:%M:%S')
        print ("invoice_date_datetime ",invoice_date_datetime)
        rate_obj = self.env['res.currency.rate']
        rate_rec = rate_obj.search([
            ('currency_id', '=', self.currency_id.id),
            ('name', '<=', str(invoice_date_datetime))
            # not sure for what purpose 'currency_rate_type_id' field exists in the table, but keep this line just in case
            ], order='name desc', limit=1)
        print ("esto es rate_rec ", rate_rec)
        if rate_rec:
            rate = rate_obj.browse(rate_rec.id).rate
        else:
            rate = 1.0
        self.tipocambio=rate

    @api.depends('amount_total', 'currency_id')
    @api.one
    def _get_amount_to_text(self):
        self.amount_to_text = amount_to_text_es_MX.get_amount_to_text(self, self.amount_total, 'es_cheque', self.currency_id.name)

    @api.model
    def _get_amount_2_text(self, amount_total):
        return amount_to_text_es_MX.get_amount_to_text(self, amount_total, 'es_cheque', self.currency_id.name)

    @api.model
    def to_json(self):
        request_params = {'company': {'rfc': self.company_id.rfc,
                     'api_key': self.company_id.api_key,
                     'modo_prueba': self.company_id.modo_prueba,
                     'regimen_fiscal': self.company_id.regimen_fiscal,
                     'postalcode': self.company_id.zip,
                     'nombre_fiscal': self.company_id.nombre_fiscal},
         'customer': {'name': self.partner_id.name,
                      'rfc': self.partner_id.rfc,
                      'residencia_fiscal': self.partner_id.residencia_fiscal,
                      'registro_tributario': self.partner_id.registro_tributario,
                      'uso_cfdi': self.uso_cfdi},
         'invoice': {'tipo_comprobante': self.tipo_comprobante,
                     'moneda': self.currency_id.name,
                     'tipocambio': self.currency_id.rate,
                     'forma_pago': self.forma_pago,
                     'methodo_pago': self.methodo_pago,
                     'subtotal': self.amount_untaxed,
                     'total': self.amount_total,
                     'folio': self.number.replace('INV', '').replace('/', ''),
                     'serie_factura': self.company_id.serie_factura},
         'adicional': {'tipo_relacion': self.tipo_relacion,
                       'uuid_relacionado': self.uuid_relacionado,
                       'confirmacion': self.confirmacion}}
        amount_total = 0.0
        amount_untaxed = 0.0
        tax_grouped = {}
        items = {'numerodepartidas': len(self.invoice_line_ids)}
        invoice_lines = []
        for line in self.invoice_line_ids:
            if line.quantity < 0:
                continue
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            amounts = line.invoice_line_tax_ids.compute_all(price, line.currency_id, line.quantity, product=line.product_id, partner=line.invoice_id.partner_id)
            price_exclude_tax = amounts['total_excluded']
            price_include_tax = amounts['total_included']
            if line.invoice_id:
                price_exclude_tax = line.invoice_id.currency_id.round(price_exclude_tax)
                price_include_tax = line.invoice_id.currency_id.round(price_include_tax)
            amount_untaxed += line.price_unit * line.quantity
            amount_total += price_include_tax
            taxes = amounts['taxes']
            tax_items = []
            product_taxes = {'numerodeimpuestos': len(taxes)}
            for tax in taxes:
                tax_id = self.env['account.tax'].browse(tax['id'])
                tax_items.append({'name': tax_id.tax_group_id.name,
                 'percentage': tax_id.amount,
                 'amount': tax['amount'],
                 'impuesto': tax_id.impuesto,
                 'tipo_factor': tax_id.tipo_factor})
                val = {'invoice_id': line.invoice_id.id,
                 'name': tax_id.tax_group_id.name,
                 'tax_id': tax['id'],
                 'amount': tax['amount']}
                key = tax['id']
                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']

            if tax_items:
                product_taxes.update({'tax_lines': tax_items})
            invoice_lines.append({'quantity': line.quantity,
             'unidad_medida': line.product_id.unidad_medida,
             'product': line.product_id.code,
             'price_unit': line.price_unit,
             'amount': line.price_unit * line.quantity,
             'description': line.name,
             'clave_producto': line.product_id.clave_producto,
             'clave_unidad': line.product_id.clave_unidad,
             'taxes': product_taxes,
             'descuento': line.price_unit * ((line.discount or 0.0) / 100.0) * line.quantity})

        request_params['invoice'].update({'subtotal': amount_untaxed,
         'total': amount_total})
        items.update({'invoice_lines': invoice_lines})
        request_params.update({'items': items})
        tax_lines = []
        tax_count = 0
        for line in tax_grouped.values():
            tax_count += 1
            tax = self.env['account.tax'].browse(line['tax_id'])
            tax_lines.append({'name': line['name'],
             'percentage': tax.amount,
             'amount': line['amount']})

        taxes = {'numerodeimpuestos': tax_count}
        if tax_lines:
            taxes.update({'tax_lines': tax_lines})
        if not self.company_id.archivo_cer:
            raise UserError(_('Archivo .cer path is missing.'))
        archivo_cer_file = open(self.company_id.archivo_cer, 'rb').read()
        if not self.company_id.archivo_key:
            raise UserError(_('Archivo .key path is missing.'))
        archivo_key_file = open(self.company_id.archivo_key, 'rb').read()
        archivo_cer = base64.b64encode(archivo_cer_file)
        archivo_key = base64.b64encode(archivo_key_file)
        request_params.update({'certificados': {'archivo_cer': archivo_cer,
                          'archivo_key': archivo_key,
                          'contrasena': self.company_id.contrasena}})
        return request_params

    #@api.multi
    #def invoice_validate(self):
        #for invoice in self:
            #if invoice.factura_cfdi:
                #values = invoice.to_json()
                #url = '%s%s' % (invoice.company_id.http_factura, '/invoice?handler=OdooHandler33')
                #response = requests.post(url, auth=None, verify=False, data=json.dumps(values), headers={'Content-type': 'application/json'})
                #json_response = response.json()
                #xml_file_link = False
                #estado_factura = json_response['estado_factura']
                #if estado_factura == 'problemas_factura':
                    #raise UserError(_(json_response['problemas_message']))
                #if json_response.get('factura_xml'):
                    #xml_file_link = invoice.company_id.factura_dir + '/' + invoice.number.replace('/', '_') + '.xml'
                    #xml_file = open(xml_file_link, 'w')
                    #xml_invoice = base64.b64decode(json_response['factura_xml'])
                    #xml_file.write(xml_invoice)
                    #xml_file.close()
                    #invoice._set_data_from_xml(xml_invoice)
                    #file_name = invoice.number.replace('/', '_') + '.xml'
                    #self.env['ir.attachment'].sudo().create({'name': file_name,
                     #'datas': json_response['factura_xml'],
                     #'datas_fname': file_name,
                     #'res_model': self._name,
                     #'res_id': invoice.id,
                     #'type': 'binary'})
                #invoice.write({'estado_factura': estado_factura,
                 #'xml_invoice_link': xml_file_link})

        #result = super(AccountInvoice, self).invoice_validate()
        #return result

    @api.one
    def _set_data_from_xml(self, xml_invoice):
        if not xml_invoice:
            return None
        NSMAP = {'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
         'cfdi': 'http://www.sat.gob.mx/cfd/3',
         'tfd': 'http://www.sat.gob.mx/TimbreFiscalDigital'}
        xml_data = etree.fromstring(xml_invoice)
        Emisor = xml_data.find('cfdi:Emisor', NSMAP)
        RegimenFiscal = Emisor.find('cfdi:RegimenFiscal', NSMAP)
        Complemento = xml_data.find('cfdi:Complemento', NSMAP)
        TimbreFiscalDigital = Complemento.find('tfd:TimbreFiscalDigital', NSMAP)
        self.rfc_emisor = Emisor.attrib['Rfc']
        self.name_emisor = Emisor.attrib['Nombre']
        self.methodo_pago = xml_data.attrib['MetodoPago']
        self.forma_pago = _(xml_data.attrib['FormaPago'])
        self.tipocambio = xml_data.attrib['TipoCambio']
        self.tipo_comprobante = xml_data.attrib['TipoDeComprobante']
        self.moneda = xml_data.attrib['Moneda']
        self.regimen_fiscal = Emisor.attrib['RegimenFiscal']
        self.numero_cetificado = xml_data.attrib['NoCertificado']
        self.cetificaso_sat = TimbreFiscalDigital.attrib['NoCertificadoSAT']
        self.fecha_certificacion = TimbreFiscalDigital.attrib['FechaTimbrado']
        self.selo_digital_cdfi = TimbreFiscalDigital.attrib['SelloCFD']
        self.selo_sat = TimbreFiscalDigital.attrib['SelloSAT']
        self.folio_fiscal = TimbreFiscalDigital.attrib['UUID']
        self.folio = xml_data.attrib['Folio']
        self.serie_emisor = xml_data.attrib['Serie']
        self.invoice_datetime = xml_data.attrib['Fecha']
        self.version = TimbreFiscalDigital.attrib['Version']
        self.cadena_origenal = '||%s|%s|%s|%s|%s||' % (self.version,
         self.folio_fiscal,
         self.fecha_certificacion,
         self.selo_digital_cdfi,
         self.cetificaso_sat)
        options = {'width': 275 * mm,
         'height': 275 * mm}
        amount_str = str(self.amount_total).split('.')
        qr_value = '?re=%s&rr=%s&tt=%s.%s&id=%s' % (self.company_id.rfc,
         self.partner_id.rfc,
         amount_str[0].zfill(10),
         amount_str[1].ljust(6, '0'),
         self.folio_fiscal)
        self.qr_value = qr_value
        ret_val = createBarcodeDrawing('QR', value=qr_value, **options)
        self.qrcode_image = base64.encodestring(ret_val.asString('jpg'))

    @api.multi
    def print_cdfi_invoice(self):
        self.ensure_one()
        filename = 'CDFI_' + self.number.replace('/', '_') + '.pdf'
        return {'type': 'ir.actions.act_url',
         'url': '/web/binary/download_document?model=account.invoice&field=pdf_cdfi_invoice&id=%s&filename=%s' % (self.id, filename),
         'target': 'self'}


    @api.one
    def action_cfdi_generate(self):
        res={}
        valida = ''
        for intentos in self.update_id:
            if intentos.name == 'factura_correcta':
                valida = 'factura_correcta'
                continue
            elif intentos.name == 'factura_cancelada':
                valida = 'factura_cancelada'
                continue
        #if self.estado_factura == 'factura_correcta':
        if valida == 'factura_correcta':
            raise UserError(_('La factura ya tiene un CFDI generado'))
        #if self.estado_factura == 'factura_cancelada':
        if valida == 'factura_cancelada':
            raise UserError(_('No puede generar un CFDI de una factura con CFDI cancelado.'))
        url = self.company_id.http_factura or False
        if not url:
            raise UserError(_('La compania no tiene configurado la URL del Web Service.'))
        cliente_cfdi = Client(url)
        #cliente_cfdi = zeep.Client(wsdl=url)
        print ("esto es el usuario",self.env.user.company_id.ip_pass)
        response = cliente_cfdi.service.procesaDocto(self.id, self.env.cr.dbname, 'ODOO', self.env.user.company_id.ip or False, self.env.user.company_id.ip_user or False, self.env.user.company_id.ip_pass or False)

        if response['codigo']=='problemas_factura':
            self.env['account.invoice.update'].create({'invoice_id':self.id, 'name':response['codigo'], 'detail':response['detalle']})
        if response['codigo']=='factura_correcta':
            self.env['account.invoice.update'].create({'invoice_id':self.id, 'name':response['codigo'], 'detail':response['detalle']})
            #raise UserError(_('Factura timbrada correctamente.'))
        return True


    @api.multi
    def action_cfdi_cancel(self):
        for invoice in self:
            if invoice.factura_cfdi:
                if invoice.estado_factura == 'factura_cancelada':
                    pass
                if not invoice.company_id.archivo_cer:
                    raise UserError(_('Falta la ruta del archivo .cer'))
                archivo_cer_file = open(invoice.company_id.archivo_cer, 'rb').read()
                if not invoice.company_id.archivo_key:
                    raise UserError(_('Falta la ruta del archivo .key'))
                archivo_key_file = open(invoice.company_id.archivo_key, 'rb').read()
                archivo_cer = base64.b64encode(archivo_cer_file)
                archivo_key = base64.b64encode(archivo_key_file)
                values = {'rfc': invoice.company_id.rfc,
                 'api_key': invoice.company_id.api_key,
                 'folio': self.folio,
                 'serie_factura': invoice.company_id.serie_factura,
                 'modo_prueba': invoice.company_id.modo_prueba,
                 'certificados': {'archivo_cer': archivo_cer,
                                  'archivo_key': archivo_key,
                                  'contrasena': invoice.company_id.contrasena}}
                url = '%s%s' % (invoice.company_id.http_factura, '/refund?handler=OdooHandler33')
                response = requests.post(url, auth=None, verify=False, data=json.dumps(values), headers={'Content-type': 'application/json'})
                json_response = response.json()
                if json_response['estado_factura'] == 'problemas_factura':
                    raise UserError(_(json_response['problemas_message']))
                elif json_response.get('factura_xml', False):
                    xml_file_link = invoice.company_id.factura_dir + '/CANCEL_' + invoice.number.replace('/', '_') + '.xml'
                    xml_file = open(xml_file_link, 'w')
                    xml_invoice = base64.b64decode(json_response['factura_xml'])
                    xml_file.write(xml_invoice)
                    xml_file.close()
                    file_name = invoice.number.replace('/', '_') + '.xml'
                    self.env['ir.attachment'].sudo().create({'name': file_name,
                     'datas': json_response['factura_xml'],
                     'datas_fname': file_name,
                     'res_model': self._name,
                     'res_id': invoice.id,
                     'type': 'binary'})
                invoice.write({'estado_factura': json_response['estado_factura']})

    @api.one
    def _get_outstanding_info_JSON(self):
        self.outstanding_credits_debits_widget = json.dumps(False)
        if self.state == 'open':
            domain = [('account_id', '=', self.account_id.id), ('partner_id', '=', self.env['res.partner']._find_accounting_partner(self.partner_id).id), ('reconciled', '=', False), ('amount_residual', '!=', 0.0)]
            if self.type in ('out_invoice', 'in_refund'):
                domain.extend([('credit', '>', 0), ('debit', '=', 0)])
                type_payment = _('Outstanding credits')
            else:
                domain.extend([('credit', '=', 0), ('debit', '>', 0)])
                type_payment = _('Outstanding debits')
            info = {'title': '', 'outstanding': True, 'content': [], 'invoice_id': self.id}
            domain.extend([('invoice_id', '!=', False)])
            lines = self.env['account.move.line'].search(domain)
            currency_id = self.currency_id
            if len(lines) != 0:
                for line in lines:
                    # get the outstanding residual value in invoice currency
                    if line.currency_id and line.currency_id == self.currency_id:
                        amount_to_show = abs(line.amount_residual_currency)
                    else:
                        amount_to_show = line.company_id.currency_id.with_context(date=line.date).compute(abs(line.amount_residual), self.currency_id)
                    if float_is_zero(amount_to_show, precision_rounding=self.currency_id.rounding):
                        continue
                    info['content'].append({
                        'journal_name': line.ref or line.move_id.name,
                        'amount': amount_to_show,
                        'currency': currency_id.symbol,
                        'id': line.id,
                        'position': currency_id.position,
                        'digits': [69, self.currency_id.decimal_places],
                    })
                info['title'] = type_payment
                self.outstanding_credits_debits_widget = json.dumps(info)
                self.has_outstanding = True

    api.one
    @api.depends('payment_move_line_ids.amount_residual')
    def _get_payment_info_JSON(self):
        print("entra aqui")
        print("esto es self",self)
        for line in self:
            print("esto es line o self",line)
            line.payments_widget = json.dumps(False)
            print("entra aqui tambien 0",line.payments_widget)
            if line.payment_move_line_ids:
                info = {'title': _('Less Payment'), 'outstanding': False, 'content': []}
                currency_id = line.currency_id
                print("entra aqui tambien 1",currency_id)

                for payment in line.payment_move_line_ids:
                    print("entra al for")
                    payment_currency_id = False


                    if line.type in ('out_invoice', 'in_refund'):
                        print("entra al tipo de factura cuando es a clientes",line.type)
                        amount = sum([p.amount for p in payment.matched_debit_ids if p.debit_move_id in line.move_id.line_ids])
                        amount_currency = sum([p.amount_currency for p in payment.matched_debit_ids if p.debit_move_id in line.move_id.line_ids])
                        if payment.matched_debit_ids:
                            
                            payment_currency_id = all([p.currency_id == payment.matched_debit_ids[0].currency_id for p in payment.matched_debit_ids]) and payment.matched_debit_ids[0].currency_id or False

                    elif line.type in ('in_invoice', 'out_refund'):
                        print("entra al tipo de factura cuando es a provedores",line.type)
                        

                        if payment.matched_debit_ids:
                            amount = sum([p.amount for p in payment.matched_debit_ids if p.debit_move_id in line.move_id.line_ids])
                            amount_currency = sum([p.amount_currency for p in payment.matched_debit_ids if p.debit_move_id in line.move_id.line_ids])
                            print ("entra al tipo de factura cuando es a provedores en el siguiente if",payment.matched_debit_ids)
                            payment_currency_id = all([p.currency_id == payment.matched_debit_ids[0].currency_id for p in payment.matched_debit_ids]) and payment.matched_debit_ids[0].currency_id or False
                                           

                        if payment.matched_credit_ids:
                            amount = sum([p.amount for p in payment.matched_credit_ids if p.credit_move_id in line.move_id.line_ids])
                            amount_currency = sum([p.amount_currency for p in payment.matched_credit_ids if p.credit_move_id in line.move_id.line_ids])
                            print ("entra al tipo de factura cuando es a provedores en el siguiente if",payment.matched_credit_ids)
                            payment_currency_id = all([p.currency_id == payment.matched_credit_ids[0].currency_id for p in payment.matched_credit_ids]) and payment.matched_credit_ids[0].currency_id or False
                    

                    # get the payment value in invoice currency
                    if payment_currency_id and payment_currency_id == line.currency_id:
                        amount_to_show = amount_currency
                    else:
                        amount_to_show = payment.company_id.currency_id.with_context(date=payment.date).compute(amount, line.currency_id)
                    if float_is_zero(amount_to_show, precision_rounding=line.currency_id.rounding):
                        continue
                    payment_ref = payment.move_id.name
                    payment_ref1 = payment.payment_id.name
                    if payment.move_id.ref:
                        payment_ref += ' (' + payment.move_id.ref + ')'
                    #info['content'].append({
                        #'name': payment.name,
                        #'journal_name': payment.journal_id.name,
                        #'amount': amount_to_show,
                        ##'currency': currency_id.symbol,
                        #'currency': payment.journal_id.currency_id.name or self.company_id.currency_id.name,
                        #'digits': [69, currency_id.decimal_places],
                        #'position': currency_id.position,
                        #'date': payment.date,
                        #'payment_id': payment.id,
                        #'pago_id': payment.payment_id.id,
                        #'move_id': payment.move_id.id,
                        #'ref': payment_ref,
                        #'ref1': payment_ref1,
                    #})
    
                    if payment.payment_id.journal_id.currency_id.name == 'USD' and line.currency_id.name == 'MXN':
                        print ("Pago USD Factura MXN")
                        #invoice_date_datetime = datetime.datetime.strptime(self.date_invoice, '%Y-%m-%d')
                        #print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = line.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', payment.payment_id.journal_id.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        print ("eso es currency_id ", currency_id)
                        print ("eso es amount_to_show ", amount_to_show)
                        amount_to_show = amount_to_show / (1/rate)
                        #invoice_date_datetime = datetime.datetime.strptime(payment.payment_id.payment_date, '%Y-%m-%d')
                        #print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = self.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', payment.payment_id.journal_id.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        amount_to_show = amount_to_show * (1/rate)
                        print ("eso es amount_to_show ", amount_to_show)
                    if payment.payment_id.journal_id.currency_id.name == 'USD' and line.currency_id.name == 'USD':
                        print ("pago USD factura en USD")
                        amount_to_show = amount_to_show
                    if (payment.payment_id.journal_id.currency_id.name == False and line.currency_id.name == 'MXN'):
                        print ("Pago MXN factura MXN ")
                        #invoice_date_datetime = datetime.datetime.strptime(self.date_invoice, '%Y-%m-%d')
                        #print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = line.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', line.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0

                        amount_to_show = amount_to_show / (1/rate)
                        #invoice_date_datetime = datetime.datetime.strptime(payment.payment_id.payment_date, '%Y-%m-%d')
                        #print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = line.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', line.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        amount_to_show = amount_to_show / (1/rate)

                    if (payment.payment_id.journal_id.currency_id.name == False and line.currency_id.name == 'USD'):
                        print ("Pago MXN Factura USD")
                        amount_to_show = amount_to_show
                        #print "Esto es amount_to_show",amount_to_show
                        #invoice_date_datetime = datetime.datetime.strptime(self.date_invoice, '%Y-%m-%d') luigi
                        #print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = line.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', line.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        #print "eso es currency_id ", currency_id
                        #print "eso es amount_to_show ", amount_to_show

                        amount_to_show = amount_to_show / (1/rate)
                       # invoice_date_datetime = datetime.datetime.strptime(payment.payment_id.payment_date, '%Y-%m-%d')
                       # print "esto es invoice_date_datetime ",invoice_date_datetime
                        rate_obj = line.env['res.currency.rate']
                        rate_rec = rate_obj.search([
                        ('currency_id', '=', line.currency_id.id)
                        #,('name', '<=', str(invoice_date_datetime))
                        ], order='name desc', limit=1)
                        if rate_rec:
                            rate = rate_obj.browse(rate_rec.id).rate
                        else:
                            rate = 1.0
                        amount_to_show = amount_to_show * (1/rate)

                    fecha_pago = str(payment.date)

                    info['content'].append({
                        'name': payment.name,
                        'journal_name': payment.journal_id.name,
                        'amount': amount_to_show,
                        #'currency': currency_id.symbol,
                        #'currency': payment.journal_id.currency_id.name or self.company_id.currency_id.name,
                        'currency': line.currency_id.name,
                        'digits': [69, currency_id.decimal_places],
                        'position': currency_id.position,
                        'date': fecha_pago,
                        'payment_id': payment.id,
                        'pago_id': payment.payment_id.id,
                        'move_id': payment.move_id.id,
                        'ref': payment_ref,
                        'ref1': payment_ref1,
                    })
                line.payments_widget = json.dumps(info)

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        print ("esto es context desde _compute_residual ", self.env.context)
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        #print "esto es sign ", sign
        for line in self.sudo().move_id.line_ids:
            print ("esto es line ", line)
            if line.account_id.internal_type in ('receivable', 'payable'):
                #print "esto es line.amount_residual ", line.amount_residual
                residual_company_signed += line.amount_residual
                #print "esto es residual_company_signed ", residual_company_signed
                #print "esto es line.currency_id ", line.currency_id
                #print "esto es self.currency_id ", self.currency_id
                if line.currency_id == self.currency_id:
                    #print "esto es line.amount_residual_currency ", line.amount_residual_currency
                    #print "esto es line.amount_residual ", line.amount_residual
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    #print "esto es from_currency ", from_currency
                    #print "esto es line.amount_residual ", line.amount_residual
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
                    print ("esto es residual ", residual)
        #print "---> ",abs(residual_company_signed) * sign
        self.residual_company_signed = abs(residual_company_signed) * sign
        #print "---> ",abs(residual) * sign
        self.residual_signed = abs(residual) * sign
        #print "---> ",abs(residual)
        self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False


class account_invoice_update(models.Model):

    _name = 'account.invoice.update'

    #@api.model
    #def create(self,vals):
            ##self.lot_id._compute_qty()
        #sale_rec = super(models.Model,self).create(vals)
        #print "esto es vals ", vals
        #print "esto es id ", sale_rec
        #self.env['account.invoice'].browse(vals.get('invoice_id')).write({'estado_factura':vals.get('name')})
        #return sale_rec

    invoice_id = fields.Many2one('account.invoice', string="CFDI", readonly=True)
    #name = fields.Char(string="Reponse")
    #code = fields.Char(string="WS Code")
    name = fields.Selection(selection=[('factura_no_generada', 'CFDI No generado'),
     ('factura_correcta', 'CFDI generado correctamente'),
     ('problemas_factura', 'Problemas en el CFDI'),
     ('factura_cancelada', 'CFDI cancelado')], string=_('Estado de factura'), default='factura_no_generada', readonly=True, copy=False)
    detail = fields.Text(string="WS Detail",readonly=True)

#class MailTemplate(models.Model):
    #"""Templates for sending email"""
    #_inherit = 'mail.template'

    #@api.multi
    #def generate_email(self, res_ids, fields = None):
        #results = super(MailTemplate, self).generate_email(res_ids, fields=fields)
        #if isinstance(res_ids, (int, long)):
            #res_ids = [res_ids]
        #res_ids_to_templates = super(MailTemplate, self).get_email_template(res_ids)
        #templates_to_res_ids = {}
        #for res_id, template in res_ids_to_templates.iteritems():
            #templates_to_res_ids.setdefault(template, []).append(res_id)

        #for template, template_res_ids in templates_to_res_ids.iteritems():
            #if template.report_template and template.report_template.report_name == 'account.report_invoice':
                #for res_id in template_res_ids:
                    #invoice = self.env[template.model].browse(res_id)
                    #if not invoice.factura_cfdi:
                        #continue
                    #if invoice.estado_factura == 'factura_correcta':
                        #xml_file = open(invoice.xml_invoice_link, 'rb').read()
                        #attachments = results[res_id]['attachments'] or []
                        #attachments.append(('CDFI_' + invoice.number.replace('/', '_') + '.xml', base64.b64encode(xml_file)))
                    #else:
                        #cancel_file_link = invoice.company_id.factura_dir + '/CANCEL_' + invoice.number.replace('/', '_') + '.xml'
                        #with open(cancel_file_link, 'rb') as cf:
                            #cancel_xml_file = cf.read()
                            #attachments = []
                            #attachments.append(('CDFI_CANCEL_' + invoice.number.replace('/', '_') + '.xml', base64.b64encode(cancel_xml_file)))
                    #results[res_id]['attachments'] = attachments

        #return results


