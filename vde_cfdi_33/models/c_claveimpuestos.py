# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_claveimpuestos (models.Model):

    _name='c.claveimpuestos'

    code = fields.Char(string='Codigo',  required=True)
    name = fields.Char(string='Descripcion',  required=True)
    retencion = fields.Selection([('1','SI'),('2','NO')], string='Retención',  required=True)
    traslado = fields.Selection([('1','SI'),('2','NO')], string='Traslado',  required=True)
    local = fields.Selection([('1','SI'),('2','NO')], string='Local',  required=True)
    federal = fields.Selection([('1','SI'),('2','NO')], string='Federal',  required=True)

