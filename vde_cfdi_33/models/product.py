#Embedded file name: C:\Program Files (x86)\Odoo 10.0\server\odoo\addons\cdfi_invoice\models\product.py
from odoo import fields, models, api, _
UM_CLAVO_MAP = {'Pieza': 'H87',
 'Hora': 'HUR',
 'Kilogramo': 'KGM',
 'Gramo': 'GRM',
 'Litro': 'LTR',
 'Galon': 'A76',
 'Tonelada': 'TNE',
 'Caja': 'XBX',
 'Metro': 'MTR',
 'Metro lineal': 'LM',
 'M2': 'MTK',
 'M3': 'MTQ',
 'Unidad de servicio': 'E48',
 'Tarifa': 'A9',
 'Dia': 'DAY',
 'Lote': 'XLT',
 'Conjunto': 'SET',
 'Actividad': 'ACT',
 'Comida': 'Q3',
 'Habitacion': 'ROM',
 'Paquete': 'XPK',
 'Mutuamente definido': 'ZZ',
 'Kit': 'KT'}
 
class ProductTemplate(models.Model):
    _inherit = 'product.product'


    @api.onchange('vde_33_producto')
    def onchange_vde_33_producto(self):
        if self.vde_33_producto:
            print ("Entro aqui")
            prodserv_obj = self.vde_33_producto
            print ("esto es prodserv_obj",prodserv_obj)
            return {'value':{'clave_producto':prodserv_obj.code}}

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    type = fields.Selection(default='product', required=True,
        help='A storable product is a product for which you manage stock. The Inventory app has to be installed.\n'
             'A consumable product is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.')
    available_in_pos = fields.Boolean(default=True)
    price_consig = fields.Float(string="Precio Consigna")
    price_mayoris = fields.Float(string="Precio Mayorista")
    UNIDAD_MEDIDA_LIST = [('Pieza', 'Pieza'),
     ('Hora', 'Hora'),
     ('Kilogramo', 'Kilogramo'),
     ('Gramo', 'Gramo'),
     ('Litro', 'Litro'),
     ('Galon', 'Galon'),
     ('Tonelada', 'Tonelada'),
     ('Caja', 'Caja'),
     ('Metro', 'Metro'),
     ('Metro lineal', 'Metro lineal'),
     ('M2', 'M2'),
     ('M3', 'M3'),
     ('Unidad de servicio', 'Unidad de servicio'),
     ('Tarifa', 'Tarifa'),
     ('Dia', 'Dia'),
     ('Lote', 'Lote'),
     ('Conjunto', 'Conjunto'),
     ('Actividad', 'Actividad'),
     ('Comida', 'Comida'),
     ('Habitacion', 'Habitacion'),
     ('Paquete', 'Paquete'),
     ('Mutuamente definido', 'Mutuamente definido'),
     ('Kit', 'Kit')]
    #unidad_medida = fields.Selection(selection=UNIDAD_MEDIDA_LIST, string='Unidad SAT')
    #clave_unidad = fields.Char(string='Clave unidad', compute='_compute_clave_unidad')
    clave_unidad = fields.Many2one('c.claveunidad', string="Unidad Medida SAT")
    manual = fields.Boolean(string='Usar Codigo directo?')
    vde_33_grupo = fields.Many2one('c.claveprodserv', string="Grupo")
    vde_33_segmento = fields.Many2one('c.claveprodserv', string="Segmento")
    vde_33_familia = fields.Many2one('c.claveprodserv', string="familia")
    vde_33_clase = fields.Many2one('c.claveprodserv', string="Clase")
    vde_33_producto = fields.Many2one('c.claveprodserv', string="Producto")
    #vde_33_code = fields.Char(string="Codigo producto" )
    clave_producto = fields.Char(string='Clave producto')

    @api.onchange('vde_33_producto')
    def onchange_vde_33_producto(self):
        prodserv_obj = self.env['c.claveprodserv']
        prodserv = prodserv_obj.browse(self.vde_33_producto)
        return {'value':{'clave_producto':prodserv.code}}

    #def onchange_vde_33_producto_(self, cr, uid, ids, vde_33_producto, context=None):
        #context = context or {}
        #result = {}
        #print "esto es context ", context
        #prodserv_obj=self.pool.get('c.claveprodserv')
        #prodserv = prodserv_obj.browse(cr, uid, vde_33_producto, context=context)
        #result.update({'vde_33_code':prodserv.code})
        #return {'value': result}

    @api.depends('unidad_medida')
    @api.one
    def _compute_clave_unidad(self):
        if self.unidad_medida:
            self.clave_unidad = UM_CLAVO_MAP[self.unidad_medida]
