# -*- coding: utf-8 -*-
from openerp import fields, models, api
import openerp.addons.decimal_precision as dp
import logging
import decimal

class AccountInvoiceLine(models.Model):

        _inherit = 'account.invoice.line'
        line_tax1 = fields.Float(compute='_compute_price_tax', string='Prix TTC',  store=True, readonly=True,)
        discount_line = fields.Float(compute='_compute_price_tax', string='Descuento por linea',  store=True, readonly=True,)
        price_subtotal_sn = fields.Float(compute='_compute_price_tax', string='Subtotal sin descuento',  store=True, readonly=True,)
        #price_subtotal = fields.Float(compute='_compute_price_tax', string='subtotal',  store=True, readonly=True,)
        line_tax2 = fields.Float(compute='_compute_price_tax', string='linea impuesto 2', store=True, readonly=True,)


        @api.one
        @api.depends('price_unit', 'discount', 'quantity','product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')

        def _compute_price_tax(self):
                _logger = logging.getLogger(__name__)
                _logger.info("Mensaje Informativo o Print")
                #_logger.info(ltax )

                for ltax_ in self.invoice_line_tax_ids:
                        ltax = (ltax_.amount or 0.0) /100
                        _logger.info(ltax_.amount)
                        if(ltax_.amount==16.0):
                                self.line_tax1 = self.price_subtotal * ltax
                                _logger.info("si entra en 16")
                        else:
                                _logger.info(str(ltax_.name));
                                if(str(ltax_.amount)=='-4.0'):
                                        self.line_tax2 = self.price_subtotal * ltax
                                        _logger.info("entra a 4")


                        #ltax = ((self.invoice_line_tax_ids.amount or 0.0) / 100)
                        #self.price_with_tax = self.price_subtotal * ltax
                self.discount_line =  (self.price_unit  * self.quantity) * (self.discount/100)
                self.price_subtotal_sn =  (self.price_unit  * self.quantity)

                #_logger = logging.getLogger(__name__)
                #_logger.info("Mensaje Informativo o Print")
                #_logger.info(self.invoice_line_tax_ids.name )
                #_logger.info(ltax )

                if self.invoice_id:
                        self.line_tax1 = self.line_tax1
                        self.discount_line = self.invoice_id.currency_id.round(self.discount_line)
                        self.line_tax2 = self.invoice_id.currency_id.round(self.line_tax2)
                        self.price_subtotal_sn = self.invoice_id.currency_id.round(self.price_subtotal_sn)
                        #self.price_subtotal = self.price_subtotal_sn
                        self.price_subtotal_signed = self.price_subtotal_sn

        @api.one
        @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity','product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id','invoice_id.date_invoice')
        def _compute_price(self):
                currency = self.invoice_id and self.invoice_id.currency_id or None
                price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
                taxes = False
                if self.invoice_line_tax_ids:
                    taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
                self.price_subtotal = price_subtotal_signed = self.quantity * price
                if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
                    price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
                sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
                self.price_subtotal_signed = price_subtotal_signed * sign
