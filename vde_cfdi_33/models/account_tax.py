#Embedded file name: C:\Program Files (x86)\Odoo 10.0\server\odoo\addons\cdfi_invoice\models\account_tax.py
from odoo import fields, models, api, _

class AccountTax(models.Model):
    _inherit = 'account.tax'
    impuesto = fields.Many2one('c.claveimpuestos', string="Impuesto")
    #impuesto = fields.Selection(selection=[('002', 'IVA'),
     #('003', ' IEPS'),
     #('001', 'ISR'),
     #('004', 'ISH')], string='Impuesto')
    tipo_factor = fields.Selection(selection=[('Tasa', 'Tasa'), ('Cuota', 'Cuota'), ('Exento', 'Exento')], string='Tipo factor')
