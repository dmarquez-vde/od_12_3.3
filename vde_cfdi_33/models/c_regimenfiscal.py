# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_regimenfiscal (models.Model):

    _name='c.regimenfiscal'

    code = fields.Char(string='Regimen Fiscal',  required=True)
    name = fields.Char(string='Descripcion',  required=True)
    moral = fields.Selection([('1','SI'),('2','NO')], string='Persona Moral',  required=True)
    fisica = fields.Selection([('1','SI'),('2','NO')], string='Persona Fisica',  required=True)
    start_life = fields.Date(string='Inicio Vigencia',  required=True)
    end_life = fields.Date(string='Fin Vigencia')