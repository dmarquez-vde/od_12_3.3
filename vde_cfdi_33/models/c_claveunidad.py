# -*- coding: utf-8 -*-
from openerp import fields,models,api,_
from datetime import date
import openerp.addons.decimal_precision as dp


class c_claveunidad (models.Model):

    _name='c.claveunidad'

    code = fields.Char(string='Clave Unidad',  required=True)
    name = fields.Char(string='Nombre', required=True)
    nota = fields.Char(string='Descripcion')
    start_life = fields.Date(string='Inicio_Vigencia',  required=True)
    end_life = fields.Date(string='Fin_Vigencia')
    sign= fields.Char(string='Simbolo')
